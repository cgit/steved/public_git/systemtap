
%{
#include <linux/kernel.h>
#include <linux/linkage.h>
#include <linux/time.h>
#include <linux/errno.h>
#include <linux/fs.h>
#include <linux/stat.h>
#include <linux/fcntl.h>
#include <linux/net.h>
#include <linux/in.h>
#include <linux/namei.h>
#include <linux/unistd.h>
#include <linux/slab.h>

#include <linux/sunrpc/clnt.h>
#include <linux/sunrpc/svc.h>
#include <linux/nfsd/nfsd.h>
%}

function nfsderror:string(err:long) 
%{
	static struct {
		int	nfserr;
		char *string;
	} nfs_errtbl[] = {
		{nfs_ok, "NFS_OK"},
		{nfserr_perm, "NFSERR_PERM"},
		{nfserr_noent, "NFSERR_NOENT"},
		{nfserr_io, "NFSERR_IO"},
		{nfserr_nxio, "NFSERR_NXIO"},
		{nfserr_eagain, "NFSERR_EAGAIN"},
		{nfserr_acces, "NFSERR_ACCES"},
		{nfserr_exist, "NFSERR_EXIST"},
		{nfserr_xdev, "NFSERR_XDEV"},
		{nfserr_nodev, "NFSERR_NODEV"},
		{nfserr_notdir, "NFSERR_NOTDIR"},
		{nfserr_isdir, "NFSERR_ISDIR"},
		{nfserr_inval, "NFSERR_INVAL"},
		{nfserr_fbig, "NFSERR_FBIG"},
		{nfserr_nospc, "NFSERR_NOSPC"},
		{nfserr_rofs, "NFSERR_ROFS"},
		{nfserr_mlink, "NFSERR_MLINK"},
		{nfserr_opnotsupp, "NFSERR_OPNOTSUPP"},
		{nfserr_nametoolong, "NFSERR_NAMETOOLONG"},
		{nfserr_notempty, "NFSERR_NOTEMPTY"},
		{nfserr_dquot, "NFSERR_DQUOT"},
		{nfserr_stale, "NFSERR_STALE"},
		{nfserr_remote, "NFSERR_REMOTE"},
		{nfserr_wflush, "NFSERR_WFLUSH"},
		{nfserr_badhandle, "NFSERR_BADHANDLE"},
		{nfserr_notsync, "NFSERR_NOT_SYNC"},
		{nfserr_badcookie, "NFSERR_BAD_COOKIE"},
		{nfserr_notsupp, "NFSERR_NOTSUPP"},
		{nfserr_toosmall, "NFSERR_TOOSMALL"},
		{nfserr_serverfault, "NFSERR_SERVERFAULT"},
		{nfserr_badtype, "NFSERR_BADTYPE"},
		{nfserr_jukebox, "NFSERR_JUKEBOX"},
		{nfserr_denied, "NFSERR_DENIED"},
		{nfserr_deadlock, "NFSERR_DEADLOCK"},
		{nfserr_expired, "NFSERR_EXPIRED"},
		{nfserr_bad_cookie, "NFSERR_BAD_COOKIE"},
		{nfserr_same, "NFSERR_SAME"},
		{nfserr_clid_inuse, "NFSERR_CLID_INUSE"},
		{nfserr_stale_clientid, "NFSERR_STALE_CLIENTID"},
		{nfserr_resource, "NFSERR_RESOURCE"},
		{nfserr_moved, "NFSERR_MOVED"},
		{nfserr_nofilehandle, "NFSERR_NOFILEHANDLE"},
		{nfserr_minor_vers_mismatch, "NFSERR_MINOR_VERS_MISMATCH"},
		{nfserr_share_denied, "NFSERR_SHARE_DENIED"},
		{nfserr_stale_stateid, "NFSERR_STALE_STATEID"},
		{nfserr_old_stateid, "NFSERR_OLD_STATEID"},
		{nfserr_bad_stateid, "NFSERR_BAD_STATEID"},
		{nfserr_bad_seqid, "NFSERR_BAD_SEQID"},
		{nfserr_symlink , "NFSERR_SYMLINK"},
		{nfserr_not_same , "NFSERR_NOT_SAME"},
		{nfserr_restorefh , "NFSERR_RESTOREFH"},
		{nfserr_attrnotsupp, "NFSERR_ATTRNOTSUPP"},
		{nfserr_bad_xdr, "NFSERR_BAD_XDR"},
		{nfserr_openmode, "NFSERR_OPENMODE"},
		{nfserr_locks_held, "NFSERR_LOCKS_HELD"},
		{nfserr_op_illegal, "NFSERR_OP_ILLEGAL"},
		{nfserr_grace, "NFSERR_GRACE"},
		{nfserr_no_grace, "NFSERR_NO_GRACE"},
		{nfserr_reclaim_bad, "NFSERR_RECLAIM_BAD"},
		{nfserr_badname, "NFSERR_BADNAME"},
		{nfserr_cb_path_down, "NFSERR_CB_PATH_DOWN"},
		{nfserr_locked, "NFSERR_LOCKED"},
		{nfserr_wrongsec, "NFSERR_WRONGSEC"},
	};
	int	i;
	int tabsz = (sizeof(nfs_errtbl)/sizeof(nfs_errtbl[0]));

	for (i = 0; i < tabsz; i++) {
		if (nfs_errtbl[i].nfserr == THIS->err) {
			break;
		}
	}
	if (i == tabsz)
		snprintf(THIS->__retvalue, MAXSTRINGLEN, "nfsderr %d", ntohl(THIS->err));
	else
		snprintf(THIS->__retvalue, MAXSTRINGLEN, 
			"nfsderr %d(%s)", ntohl(nfs_errtbl[i].nfserr), nfs_errtbl[i].string);

%}

/*
 * stap -g -I../tapset lockd_clnt.stp
 */

%{
#include <linux/sunrpc/svc.h>
#include <linux/sunrpc/sched.h>
#include <linux/sunrpc/clnt.h>
#include <linux/lockd/lockd.h>
#include <linux/lockd/nlm.h>
#include <linux/lockd/xdr.h>

enum {
    NSMPROC_NULL,
    NSMPROC_STAT,
    NSMPROC_MON,
    NSMPROC_UNMON,
    NSMPROC_UNMON_ALL,
    NSMPROC_SIMU_CRASH,
    NSMPROC_NOTIFY,
};
%}

function task_nlm_error:string(_task:long)
%{
	struct rpc_task *task = (struct rpc_task *)(long) kread(&(THIS->_task));
	char *errstr;

	switch(task->tk_status) {
	case NLM_LCK_GRANTED:
		errstr = "GRANTED";
		break;
	case NLM_LCK_DENIED:
		errstr = "DENIED";
		break;
	case NLM_LCK_DENIED_NOLOCKS:
		errstr = "NOLOCKS";
		break;
	case NLM_LCK_BLOCKED:
		errstr = "BLOCKED";
		break;
	case NLM_LCK_DENIED_GRACE_PERIOD:
		errstr = "GRACE";
		break;
	default:
		errstr = "Unknown Error";
	};
	snprintf(THIS->__retvalue, MAXSTRINGLEN, "task status %d (%s)",
		task->tk_status, errstr); 

	CATCH_DEREF_FAULT();
%}

/*
function rpcprocnum:long(_msg:long)
%{
	struct rpc_message *msg = (struct rpc_message *)(long) kread(&(THIS->_msg));
	struct rpc_procinfo *rpc_proc = 
		(struct rpc_procinfo *)(long) kread(&(msg->rpc_proc));

	THIS->__retvalue = rpc_proc->p_proc;

	CATCH_DEREF_FAULT();
%}
function rpcprocname:string(_msg:long)
%{
	struct rpc_message *msg = (struct rpc_message *)(long) kread(&(THIS->_msg));
	struct rpc_procinfo *rpc_proc = 
		(struct rpc_procinfo *)(long) kread(&(msg->rpc_proc));
	char *p_name = kread(&(rpc_proc->p_name));
	char buf[MAXSTRINGLEN];

	snprintf(THIS->__retvalue, MAXSTRINGLEN, "%s(%d)",
		p_name ? p_name : "NULL" , rpc_proc->p_proc);

	CATCH_DEREF_FAULT();
%}
*/
function nlmproc:string(proc:long) 
%{
	char *procstr;

	switch(THIS->proc) {
	case NLMPROC_NULL:
		procstr = "NULL";
		break;
	case NLMPROC_TEST:
		procstr = "TEST";
		break;
	case NLMPROC_LOCK:
		procstr = "LOCK";
		break;
	case NLMPROC_CANCEL:
		procstr = "CANCEL";
		break;
	case NLMPROC_UNLOCK:
		procstr = "UNLOCK";
		break;
	case NLMPROC_GRANTED:
		procstr = "GRANTED";
		break;
	default:
		procstr = "Bad Proc";
	};

	snprintf(THIS->__retvalue, MAXSTRINGLEN, "%s", procstr);
%}
function nmsproc:string(proc:long) 
%{
	char *procstr;

	switch(THIS->proc) {
	case NSMPROC_NULL:
		procstr = "NULL";
		break;
	case NSMPROC_STAT:
		procstr = "STAT";
		break;
	case NSMPROC_MON:
		procstr = "MON";
		break;
	case NSMPROC_UNMON:
		procstr = "UNMON";
		break;
	case NSMPROC_UNMON_ALL:
		procstr = "UNMON_ALL";
		break;
	case NSMPROC_SIMU_CRASH:
		procstr = "SIMU_CRASH";
		break;
	case NSMPROC_NOTIFY:
		procstr = "NOTIFY";
		break;
	default:
		procstr = "Bad Proc";
	};

	snprintf(THIS->__retvalue, MAXSTRINGLEN, "%s", procstr);
%}

function nlm_host:string(_host:long) 
%{
	struct nlm_host *host = (struct nlm_host *)(long) kread(&(THIS->_host));
	char *h_name = kread(&(host->h_name));

	snprintf(THIS->__retvalue, MAXSTRINGLEN, "host %s", h_name); 

	CATCH_DEREF_FAULT();
%}
function nlm_res_status:long(_req:long) 
%{
	struct nlm_rqst *req = (struct nlm_rqst *)(long) kread(&(THIS->_req));

	THIS->__retvalue = req->a_res.status;

	CATCH_DEREF_FAULT();
%}
function nlm_res_error:string(_req:long)
%{
	struct nlm_rqst *req = (struct nlm_rqst *)(long) kread(&(THIS->_req));
	char *errstr;

	switch(req->a_res.status) {
	case nlm_granted:
		errstr = "GRANTED";
		break;
	case nlm_lck_denied:
		errstr = "DENIED";
		break;
	case nlm_lck_denied_nolocks:
		errstr = "NOLOCKS";
		break;
	case nlm_lck_blocked:
		errstr = "BLOCKED";
		break;
	case nlm_lck_denied_grace_period:
		errstr = "GRACE";
		break;
	default:
		errstr = "Unknown Error";
	};
	snprintf(THIS->__retvalue, MAXSTRINGLEN, " Server: %s", errstr); 

	CATCH_DEREF_FAULT();
%}

global nlm_req, nms_proc
/* global msg, flags */

probe module("lockd").function("nlmclnt_call")
{
	printf("%s(%d): nlmclnt_call: proc %s \n", 
		execname(), pid(), nlmproc($proc));
	nlm_req = $req;
}
probe module("lockd").function("nlmclnt_call").return
{
	if ($return) {
		printf("%s(%d): nlmclnt_call: ERROR: %d (%s)\n", 
			execname(), pid(), $return, errno_str($return));
	}
	if (nlm_res_status(nlm_req)) {
		printf("%s(%d): nlmclnt_call: ERROR: %s\n", 
			execname(), pid(), nlm_res_error(nlm_req));
	}
}
probe module("lockd").function("nlmclnt_async_call")
{
	printf("%s(%d): nlmclnt_async_call: proc %s \n", 
		execname(), pid(), nlmproc($proc));
}
probe module("lockd").function("nlmclnt_async_call").return
{
	if ($return) {
		printf("%s(%d): nlmclnt_async_call: %d (%s)\n", 
			execname(), pid(), $return, errno_str($return));
	}
}
probe module("lockd").function("nsm_mon_unmon")
{
	nms_proc = $proc;
	/*
	printf("%s(%d): nsm_mon_unmon: %s\n", execname(), pid(), 
		nmsproc($proc));
	*/
}
probe module("lockd").function("nsm_monitor").return
{
	if ($return) {
		printf("%s(%d): nsm_monitor: %s: error %d (%s)\n", 
			execname(), pid(), nmsproc(nms_proc),
			$return, errno_str($return));
	}
}
probe module("lockd").function("nsm_mon_unmon").return
{
	if ($return) {
		printf("%s(%d): nsm_mon_unmon: return %d (%s)\n", 
			execname(), pid(), $return, errno_str($return));
	}
}
probe module("lockd").function("nlmclnt_unlock_callback")
{
	if (task_status($task)) {
		printf("%s(%d): nlmclnt_unlock_callback: ERROR: %s \n", 
			execname(), pid(), task_nlm_error($task));
	}
}
probe module("lockd").function("nlm_rebind_host")
{
	printf("%s(%d): nlm_rebind_host: %s \n", 
			execname(), pid(), nlm_host($host));
}
probe module("lockd").function("nlm_bind_host")
{
	printf("%s(%d): nlm_bind_host: %s \n", 
			execname(), pid(), nlm_host($host));
}
/*
probe module("sunrpc").function("rpc_call_sync")
{
	printf("%s(%d): rpc_call_sync: %s: flags 0x%x\n",
			execname(), pid(), rpcprocname($msg), $flags);
	msg = $msg;
	flags = $flags
}
probe module("sunrpc").function("rpc_call_sync").return
{
	if ($return) {
		if (rpcprocnum(msg) != 2 && $return != -2) {
			printf("%s(%d): rpc_call_sync: %s: return %d (%s)\n", 
				execname(), pid(), rpcprocname(msg), $return, 
				errno_str($return));
		}
	}
}
probe module("lockd").function("nsm_monitor")
{
	printf("%s(%d): nsm_nmonitor: host %p\n", 
		execname(), pid(), $host);
}
probe module("lockd").function("nsm_unmonitor")
{
	printf("%s(%d): nsm_unmonitor: host %p\n", 
		execname(), pid(), $host);
}
*/
probe begin { log("starting lockd probe") }
probe end { log("ending lockd probe") }

